require 'rails_helper'


RSpec.describe StaticPagesController, :type => :requests do
  
  let(:title) { "Ruby on Rails Tutorial Sample App" }

  
  describe "Home page" do

    before { visit root_path }

    it { expect(page).to have_selector('h1', :text => 'Sample App') }
    it { expect(page).to have_title(title) }
    it { expect(page).to_not have_title("| Home") }
  end
  
  describe "Help page" do

    before { visit help_path }
    
    it { expect(page).to have_selector('h1', :text => 'Help') }
    it { expect(page).to have_title("Help | #{title}") }
  end
  
  describe "About page" do

    before { visit about_path }
    
    it { expect(page).to have_selector('h1', :text => 'About') }
    it { expect(page).to have_title("About | #{title}") }
  end

  describe "Contact page" do

    before { visit contact_path }
    
    it { expect(page).to have_selector('h1', :text => 'Contact') }
    it { expect(page).to have_title("Contact | #{title}") }
  end
end
