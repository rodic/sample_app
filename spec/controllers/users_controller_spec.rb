require 'rails_helper'

RSpec.describe UsersController, :type => :requests do

  describe "signup page" do
    
    before { visit signup_path }

    it { expect(page).to have_content('Sign up') }
    it { expect(page).to have_title('Sign up')   }
  end
end
