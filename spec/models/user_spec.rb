require 'rails_helper'

RSpec.describe User, :type => :model do
  let(:user) { User.new(name: "Example User", email: "user@example.com",
                        password: "foobar", password_confirmation: "foobar") }

  describe "User with valid name and valid email" do
    it { expect(user.valid?).to be_truthy }
  end

  describe "Name should be present" do
    before { user.name = " " }
    it { expect(user.valid?).to be_falsey }
  end

  describe "Email should be present" do
    before { user.email = " " }
    it { expect(user.valid?).to be_falsey }
  end

  describe "Name should not be too long" do
    before { user.name = "a" * 51 }
    it { expect(user.valid?).to be_falsey }
  end

  describe "Email should not be too long" do
    before { user.email = "a" * 256 }
    it { expect(user.valid?).to be_falsey }
  end

  describe "Email validation should accept valid addresses" do
    valid_addresses = %w{user@example.org USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn}

    valid_addresses.each do |valid_address|
      before { user.email = valid_address }
      it { expect(user.valid?).to be_truthy }
    end
  end

  describe "Email validation should reject invalid addresses" do
    invalid_addresses = %w{@example.org USERfoo.COM A_US!ER@foo.bar.org}

    invalid_addresses.each do |invalid_address|
      before { user.email = invalid_address }
      it { expect(user.valid?).to be_falsey }
    end
  end

  describe "Email should be unique" do
    before { User.create(name: "Another User", email: user.email.upcase,
                         password: user.password,
                         password_confirmation: user.password_confirmation) }
    it { expect(user.save).to be_falsey }
  end

  describe "Password should have a minimum length" do
    before do
      user.password = "a" * 5
      user.password_confirmation = "a" * 5
    end
    it { expect(user.save).to be_falsey }
  end
end
